const mongoose = require('mongoose');
const mongoUri = require('../config').mongoUri;
const Users = require('../models/users');
const Transaction = require('../models/transactions');
const cron = require('node-cron');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: 'smtp.office365.com',
  port: 587,
  auth: {
    user: 'shane@wealthconditioner.com',
    pass: 'jU$tiC3!!',
  },
  secure: false,
  tls: {
    ciphers: 'SSLv3'
  }
});

const mailOpts = {
  from: 'shane@wealthconditioner.com',
  to: 'shane.provost@yahoo.com',
  subject: 'New Deposit Submitted',
  html: '<p>Hello!<br><br>$1,000 was successfully deposited into your Wealth Conditioner account.<br><br>You now have a balance of <strong>$1,033</strong>*.<br><br>Thank you for your continued use!<br><br>The Wealth Conditioner Team<br><br><br><br><br><br><small>*Value displayed is for imaginary purposes only.</small>',
};
transporter.sendMail(mailOpts, (err, info) => {
  if (err) {
    console.log(err);
  } else {
    console.log(`Email to shane.provost@yahoo.com successful`);
  }
});
