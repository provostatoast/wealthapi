const mongoose = require('mongoose');
const mongoUri = require('../config').mongoUri;
const Users = require('../models/users');
const Transaction = require('../models/transactions');
const cron = require('node-cron');
const nodemailer = require('nodemailer');

const transporter = nodemailer.createTransport({
  host: 'smtp.office365.com',
  port: 587,
  auth: {
    user: 'shane@wealthconditioner.com',
    pass: 'jU$tiC3!!',
  },
  secure: false,
  tls: {
    ciphers: 'SSLv3'
  }
});

mongoose.connect(mongoUri, {}, function(err) {
  if (err) console.log('ERROR ESTABLISHING DB CONNECTION');

  console.log('CONNECTED TO DB');
});

// cron.schedule('59 23 * * *', function() {
  // loop through users and update their balances
Users.find({}, (err, users) => {
  users.forEach((user) => {
    const incrementCount = user.count += 1;
    const id = user.id;
    const email = user.email;
    const balanceIncrease = incrementCount * 1000;
    const formattedIncrease = balanceIncrease.toLocaleString();
    const balance = user.balance + balanceIncrease;
    const formattedBalance = balance.toLocaleString();
    const Deposit = Transaction({
      type: 'Deposit',
      user: id,
      amount: balanceIncrease,
    });
    Users.findOneAndUpdate(
      { id },
      { $set: { count: incrementCount, balance } }, (err, resp) => {
      console.log(resp.balance);
    });

    Deposit.save((err) => {
      if (err) throw err;
      console.log('Deposit successful');
      const mailOpts = {
        from: 'shane@wealthconditioner.com',
        to: email,
        subject: 'New Deposit Submitted',
        html: `<p>Hello!<br><br>$${formattedIncrease} was successfully deposited into your Wealth Conditioner account.<br><br>You now have a balance of <strong>$${formattedBalance}</strong>*. Check it out at <a href="http://www.wealthconditioner.com">wealthconditioner.com</a>.<br><br>Thank you for your continued use!<br><br>The Wealth Conditioner Team<br><br><br><br><br><br><small>*Value displayed is for imaginary purposes only.</small><br><small>Want to stop receiving updates? Simply reply back to this email requesting removal, and updates will stop within two days. Thanks!</small>`,
      };
      transporter.sendMail(mailOpts, (err, info) => {
        if (err) {
          console.log(err);
        } else {
          console.log(`Email to ${email} successful`);
        }
      });
    });
  });
});
// });
