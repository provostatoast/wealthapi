const express = require('express');
const authCheck = require('../auth/authCheck');
const router = express.Router();
const moment = require('moment');

const Users = require('../models/users');
const Deposits = require('../models/deposits');
const Withdrawal = require('../models/withdrawals');
const Transaction = require('../models/transactions');
const Debt = require('../models/debts');

/* GET user data. */
router.get('/users/:id?', (req, res, next) => {
  const id = req.params.id;
  Users.find({ id }, (err, resp) => {
    res.send(resp);
  });
});

/*
  Check if user exists, if not, create them and add their first deposit
*/
router.post('/users/', authCheck, (req, res, next) => {
  const id = req.body.user_id.replace(/auth0\|/, '');
  const email = req.body.email;
  const username = req.body.nickname;
  Users.find({ id }, (err, resp) => {
    if (!err && resp.length) {
      return res.send(resp[0]);
    }
    const newUser = Users({
      id,
      email,
      username,
    });
    const Deposit = Transaction({
      type: 'Deposit',
      user: id,
      amount: 1000,
    });

    newUser.save((err, user) => {
      if (err) console.log('Error saving new user to the Db');
      res.send(user);
      Deposit.save((err) => {
        if (err) throw err;
        return console.log('We saved the transaction');
      });
    });
  });
});

router.post('/users/:id/balance', (req, res, next) => {
  const id = req.params.id;
  const balance = req.body.balance;
  Users.findOneAndUpdate({ id }, { balance }, (err, resp) => {
    if (err) console.log('Error updating user balance');
    res.send(resp);
  })
})

router.get('/transactions/:id?', function(req, res, next) {
  const user = req.params.id;
  Transaction.find({ user }).exec((err, docs) => {
    res.send(docs);
  });
});

router.post('/transactions', authCheck, (req, res) => {
  const userId = req.body.userId;
  const nt = req.body;
  console.log(nt);
  const t = Transaction({
    type: nt.type,
    user: nt.user,
    title: nt.title,
    url: nt.url,
    amount: nt.amount,
    date: nt.date,
  });
  t.save(err => {
    if (err) console.log(`Error saving transaction: ${err}`);
    res.json({ success: 'Transaction Saved' });
  })
});

router.post('/debts', authCheck, (req, res) => {
  const userId = req.body.userId;
  const balance = req.body.balance;
  const name = req.body.name;

  const d = Debt({
    userId,
    balance,
    name,
  });

  d.save(err => {
    if (err) console.log(`Error saving debt: ${err}`);
    Debt.find({ userId }, (e, allDebts) => {
      if (e) throw e;
      res.send(allDebts);
    });
  });
});

router.post('/debts/update', authCheck, (req, res) => {
  const _id = req.body._id;
  const userId = req.body.userId;
  const balance = req.body.balance;
  Debt.findOneAndUpdate({ _id }, { $set: { balance } }, (err, doc) => {
    if (err) throw err;
    Debt.find({ userId }).exec((e, docs) => {
      if (e) throw e;
      res.send(docs);
    });
  })
});

router.get('/debts/:id?', (req, res, next) => {
  const userId = req.params.id;
  Debt.find({ userId }).exec((err, docs) => {
    res.send(docs);
  });
});

router.post('/debts/remove/:id', (req, res, next) => {
  const debtId = req.params.id;
  const userId = req.body.userId;

  Debt.find({ _id: debtId })
    .remove()
    .exec((err) => {
      if (err) throw err;
      Debt.find({ userId }, (e, allDebts) => {
        if (e) throw e;
        res.send(allDebts);
      });
    });
});

module.exports = router;
