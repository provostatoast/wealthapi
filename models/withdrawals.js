// user model here
const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const WithdrawalSchema = new Schema({
  type: { type: String },
  user: { type: Number },
  title: { type: String },
  url: { type: String },
  amount: { type: Number },
  date: { type: Number },
});

module.exports = mongoose.model('Withdrawal', WithdrawalSchema, 'withdrawals');
