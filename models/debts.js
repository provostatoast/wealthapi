// user model here
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DebtSchema = new Schema({
  userId: { type: String, required: true },
  name: { type: String, default: '' },
  balance: { type: Number }
});

module.exports = mongoose.model('Debts', DebtSchema, 'debts');
