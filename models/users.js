// user model here
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
  id: { type: String, required : true, dropDups: true, required: true },
  name: { type: String, default: '' },
  username: { type: String, default: '' },
  email: { type: String },
  balance: { type: Number, default: 1000 },
  count: { type: Number, default: 1 },
  lastLogin: { type: Number, default: Math.round((new Date()).getTime() / 1000) },
});

module.exports = mongoose.model('User', UserSchema, 'users');
