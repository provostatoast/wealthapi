// user model here
const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const DepositSchema = new Schema({
  type: { type: String },
  user: { type: Number, required: true },
  amount: { type: Number, required: true },
  date: { type: Number, required: true },
});

module.exports = mongoose.model('Deposit', DepositSchema, 'deposits');
