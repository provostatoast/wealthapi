// user model here
const mongoose = require('mongoose');
const moment = require('moment');

const Schema = mongoose.Schema;

const TransactionSchema = new Schema({
  type: { type: String, default: 'Deposit' },
  user: { type: String, required: true },
  title: { type: String, default: 'Daily Deposit' },
  url: { type: String, default: '', required: false },
  amount: { type: Number, default: 1000 },
  date: { type: Number, default: Math.round((new Date()).getTime() / 1000) },
});

module.exports = mongoose.model('Transaction', TransactionSchema, 'transactions');
