const jwt = require('express-jwt');
const jwks = require('jwks-rsa');

module.exports = jwt({
  // Dynamically provide a signing key
  // based on the kid in the header and
  // the singing keys provided by the JWKS endpoint.
  secret: jwks.expressJwtSecret({
    cache: true,
    rateLimit: true,
    jwksRequestsPerMinute: 5,
    jwksUri: `https://weathconditioner.auth0.com/.well-known/jwks.json`
  }),

  // Validate the audience and the issuer.
  audience: '0UGS5CNBYkCCAHSGYccEdEiZ13Ev6La9',
  issuer: `https://weathconditioner.auth0.com/`,
  algorithms: ['RS256']
});
